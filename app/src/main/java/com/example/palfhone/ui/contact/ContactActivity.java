package com.example.palfhone.ui.contact;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.example.palfhone.R;
import com.example.palfhone.ui.contact.pad.PadFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class ContactActivity extends AppCompatActivity implements PadFragment.AlarmToActivity {


    private BottomNavigationView bottomNavigationView;
    private static final int CONTENT_VIEW_ID = 10101010;
    private FrameLayout frameFragment;
    private View view1;
    private RelativeLayout rtl_nav;
    private RelativeLayout rtl_pad_row5;

    int a = 0;
    private int lastIconSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        rtl_nav = findViewById(R.id.rtl_nav);
        view1 = findViewById(R.id.view1);
        rtl_pad_row5 = findViewById(R.id.rtl_pad_row5);
        bottomNavigationView.setItemIconTintList(null); //remove title from botton navigation


        //start activity by pad fragment
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame_main, new PadFragment(ContactActivity.this), "11")
                .addToBackStack(null)
                .commit();


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                setIconsUnclicked();
                switch (item.getItemId()) {
                    case R.id.nav_pad1:
                        item.setIcon(R.drawable.ic_btnnavone_clicked);
                        break;
                    case R.id.nav_pad2:
                        item.setIcon(R.drawable.ic_btnnavtwo_clicked);

                        break;
                    case R.id.nav_pad3:
                        item.setIcon(R.drawable.ic_btnnavthree_clicked);

                        break;
                    case R.id.nav_pad4:
                        item.setIcon(R.drawable.ic_btnnavfour_clicked);

                        break;
                    case R.id.nav_pad5:
                        item.setIcon(R.drawable.ic_btnnavfive_clicked);

                        break;

                }
                return false;
            }
        });
    }


    //set all items in unclicked mode
    private void setIconsUnclicked() {
        bottomNavigationView.getMenu().getItem(0).setIcon(R.drawable.ic_btnnavone_unclicked);
        bottomNavigationView.getMenu().getItem(1).setIcon(R.drawable.ic_btnnavtwo_unclicked);
        bottomNavigationView.getMenu().getItem(2).setIcon(R.drawable.ic_btnnavthree_unclicked);
        bottomNavigationView.getMenu().getItem(3).setIcon(R.drawable.ic_btnnavfour_unclicked);
        bottomNavigationView.getMenu().getItem(4).setIcon(R.drawable.ic_btnnavfive_unclicked);
    }

    @Override
    public void onHide(Boolean x) {
        if (x) {

            if (a==0) {
                Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha_animation);
                //rtl_nav.startAnimation(anim);
                rtl_nav.animate().translationY(300).setDuration(500);
                rtl_nav.setY(300);
                rtl_pad_row5.animate().translationY(0);
                a=1;
                view1.setBackgroundColor(Color.parseColor("#031E29"));


            }


        } else {
            if (a==1) {

                rtl_nav.setVisibility(View.VISIBLE);
                a=0;
                rtl_nav.animate().translationY(0).setDuration(500);
                rtl_nav.setY(0);
                rtl_pad_row5.animate().translationY(400);

                Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha_animaiton_hide_to_show );
                rtl_nav.startAnimation(anim);
                view1.setBackgroundColor(Color.parseColor("#666666"));


            }


        }
    }
}