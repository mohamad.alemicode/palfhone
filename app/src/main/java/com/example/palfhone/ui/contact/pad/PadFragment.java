package com.example.palfhone.ui.contact.pad;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.palfhone.R;
import com.example.palfhone.ui.contact.ContactActivity;
import com.example.palfhone.ui.contact.addcontact.AddContactActivity;


public class PadFragment extends Fragment {

    //pad for row1
    private RelativeLayout rtl_pad_one;
    private RelativeLayout rtl_pad_two1;
    private RelativeLayout rtl_pad_three1;

    //pad for row2
    private RelativeLayout rtl_pad_one2;
    private RelativeLayout rtl_pad_two2;
    private RelativeLayout rtl_pad_three2;

    //pad for row3
    private RelativeLayout rtl_pad_one3;
    private RelativeLayout rtl_pad_two3;
    private RelativeLayout rtl_pad_three3;


    //pad for row4
    private RelativeLayout rtl_pad_one4;
    private RelativeLayout rtl_pad_two4;
    private RelativeLayout rtl_pad_three4;
    private RelativeLayout rtl_top;


    private RelativeLayout rtl_pad_row5;

    private ImageView iv_back;
    private TextView tv_contact_addToContact;
    private EditText edt_number;

    private String inputNum = "";
    private static final String TAG = "mohamad";
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;


    private AlarmToActivity alarmToActivity;

    private View view;

    public PadFragment(ContactActivity contactActivity) {

        alarmToActivity = contactActivity;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_pad, container, false);


        rtl_pad_one = view.findViewById(R.id.rtl_pad_one);
        rtl_pad_two1 = view.findViewById(R.id.rtl_pad_two1);
        rtl_pad_three1 = view.findViewById(R.id.rtl_pad_three1);

        rtl_pad_one2 = view.findViewById(R.id.rtl_pad_one2);
        rtl_pad_two2 = view.findViewById(R.id.rtl_pad_two2);
        rtl_pad_three2 = view.findViewById(R.id.rtl_pad_three2);

        rtl_pad_one3 = view.findViewById(R.id.rtl_pad_one3);
        rtl_pad_two3 = view.findViewById(R.id.rtl_pad_two3);
        rtl_pad_three3 = view.findViewById(R.id.rtl_pad_three3);


        rtl_pad_one4 = view.findViewById(R.id.rtl_pad_one4);
        rtl_pad_two4 = view.findViewById(R.id.rtl_pad_two4);
        rtl_pad_three4 = view.findViewById(R.id.rtl_pad_three4);
        rtl_top = view.findViewById(R.id.rtl_top);
        rtl_pad_row5 = view.findViewById(R.id.rtl_pad_row5);


        iv_back = view.findViewById(R.id.iv_back);

        edt_number = view.findViewById(R.id.edt_number);
        tv_contact_addToContact = view.findViewById(R.id.tv_contact_addToContact);


        tv_contact_addToContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), AddContactActivity.class));
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Boolean lastcharIsSpace = false;

                if (inputNum.charAt(inputNum.length() - 1) == ' ') {
                    lastcharIsSpace = true;
                } else {
                    lastcharIsSpace = false;
                }

                if (inputNum != null && inputNum.length() > 0) { // check to remove last item
                    inputNum = inputNum.substring(0, inputNum.length() - 1);
                    if (lastcharIsSpace) { // check if after back there is space it should be remove too
                        inputNum = inputNum.substring(0, inputNum.length() - 1);
                    }

                    edt_number.setText(inputNum);

                    setNumber(inputNum, true);
                    onHideTop(inputNum, true);
                }

            }
        });
        rtl_pad_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputNum += "1";

                inputNum = setNumber(inputNum, false);


                edt_number.setText(inputNum);


            }
        });


        rtl_pad_two1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputNum += "2";

                inputNum = setNumber(inputNum, false);


                edt_number.setText(inputNum);
            }
        });

        rtl_pad_three1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputNum += "3";

                inputNum = setNumber(inputNum, false);

                edt_number.setText(inputNum);


            }
        });
        rtl_pad_three2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputNum += "6";

                inputNum = setNumber(inputNum, false);


                edt_number.setText(inputNum);


            }
        });
        rtl_pad_one2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputNum += "4";

                inputNum = setNumber(inputNum, false);


                edt_number.setText(inputNum);


            }
        });
        rtl_pad_two2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputNum += "5";

                inputNum = setNumber(inputNum, false);


                edt_number.setText(inputNum);


            }
        });

        rtl_pad_two3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputNum += "8";

                inputNum = setNumber(inputNum, false);


                edt_number.setText(inputNum);


            }
        });

        rtl_pad_one3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputNum += "7";

                inputNum = setNumber(inputNum, false);


                edt_number.setText(inputNum);


            }
        });

        rtl_pad_three3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputNum += "9";

                inputNum = setNumber(inputNum, false);


                edt_number.setText(inputNum);


            }
        });

        rtl_pad_one4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputNum += "+";

                inputNum = setNumber(inputNum, false);


                edt_number.setText(inputNum);


            }
        });

        rtl_pad_two4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputNum += "0";

                inputNum = setNumber(inputNum, false);


                edt_number.setText(inputNum);


            }
        });

        rtl_pad_three4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputNum += "#";

                inputNum = setNumber(inputNum, false);


                edt_number.setText(inputNum);


            }
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    //this method will be called after each tab (number or back)
    private String setNumber(String number, Boolean callByBack) {

        switch (number.length()) {
            case 3:
                number += " ";
                break;
            case 7:
                number += " ";
                break;
            case 11:
                number += " ";
                break;
            case 14:
                number += " ";
                break;
        }
        if (number.length() > 0) {
            iv_back.setVisibility(View.VISIBLE);
        } else {
            iv_back.setVisibility(View.GONE);

        }
        if (number.length() >= 3) {

            alarmToActivity.onHide(true);
            tv_contact_addToContact.setVisibility(View.VISIBLE);

            // rtl_pad_row5.animate().translationY(0);

        } else if (number.length() <= 2) {

            alarmToActivity.onHide(false);
            tv_contact_addToContact.setVisibility(View.GONE);


        }
        onHideTop(inputNum, callByBack);


        return number;

    }


    //hide items on top of pad number
    private void onHideTop(String number, Boolean callByBack) {
        if (number.length() >= 16 && rtl_top.getVisibility() == View.VISIBLE) {
            Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.alpha_animation);
            rtl_top.startAnimation(anim);
            rtl_top.setVisibility(View.GONE);
        } else if (number.length() < 16 && rtl_top.getVisibility() == View.GONE) {
            rtl_top.setVisibility(View.VISIBLE);
            Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.alpha_animaiton_hide_to_show);
            rtl_top.startAnimation(anim);

        }
    }

    interface AlarmToActivity {
        void onHide(Boolean x); //hide botton navigation
    }


}